class ganglia (
    String $package_name,
    String $package_ensure,
    Optional[String] $package_provider = undef,
    String $service_name,
    String $service_ensure,
    String $service_user,
    String $cluster_name,
    String $cluster_owner,
    String $cluster_latlong,
    String $cluster_url,
    String $module_path,
    String $conf_file,
    String $location,
    Optional[String] $allow_extra_data = undef,
    Optional[Boolean] $override_hostname = false,
    Integer $cleanup_threshold,
    Integer $send_metadata_interval,
    Integer $host_dmax,
    Integer $host_tmax,
    Boolean $service_enable,
    Boolean $mute,
    Boolean $deaf,
    Array[Hash] $udp_send_channel,
    Array[Hash] $udp_recv_channel,
    Array[Hash] $tcp_accept_channel,
) {
    if $package_provider {
	Package {
	    provider => $package_provider,
	}
    }

    package{'ganglia-gmond':
        name => $package_name,
	ensure => $package_ensure,
    }

    service { 'gmond':
	name => $service_name,
	ensure => $service_ensure,
	enable => $service_enable,
	require => Package['ganglia-gmond'],
    }

    file {'gmond.conf':
        path => "${conf_file}",
	ensure => file,
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('ganglia/etc/ganglia/gmond.conf.epp', {
	    'service_user' => $service_user,
	    'cluster_name' => $cluster_name,
	    'cluster_owner' => $cluster_owner,
	    'cluster_latlong' => $cluster_latlong,
	    'cluster_url' => $cluster_url,
	    'mute' => $mute,
	    'deaf' => $deaf,
	    'location' => $location,
	    'cleanup_threshold' => $cleanup_threshold,
	    'send_metadata_interval' => $send_metadata_interval,
	    'udp_send_channel' => $udp_send_channel,
	    'udp_recv_channel' => $udp_recv_channel,
	    'tcp_accept_channel' => $tcp_accept_channel,
	    'module_path' => $module_path,
            'host_dmax' => $host_dmax,
            'host_tmax' => $host_tmax,
            'allow_extra_data' => $allow_extra_data,
            'override_hostname' => $override_hostname,
	}),
	require => Package['ganglia-gmond'],
	notify => Service['gmond'],
    }

}
