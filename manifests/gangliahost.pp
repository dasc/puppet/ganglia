class ganglia::gangliahost (
    String $package_name,
    String $package_ensure,
    Optional[String] $package_provider = undef,
    String $service_name,
    String $service_ensure,
    String $service_user,
    String $cluster_name,
    String $cluster_owner,
    String $cluster_latlong,
    String $cluster_url,
    String $module_path,
    Optional[String] $allow_extra_data = undef,
    Optional[Boolean] $override_hostname = false,
    String $conf_file,
    String $location,
    String $gmetad_username,
    String $gridname,
    Array[String] $trusted_hosts,
    Integer $cleanup_threshold,
    Integer $send_metadata_interval,
    Integer $host_dmax,
    Integer $host_tmax,
    Boolean $service_enable,
    Boolean $mute,
    Boolean $deaf,
    Array[Hash] $udp_send_channel,
    Array[Hash] $udp_recv_channel,
    Array[Hash] $tcp_accept_channel,
    Array[Hash] $data_source,
) {
    if $package_provider {
	Package {
	    provider => $package_provider,
	}
    }

    package{'ganglia-gmetad':
        name => 'ganglia-gmetad',
        ensure => $package_ensure,
    }
    
    service { 'gmetad':
	name => 'gmetad',
	ensure => $service_ensure,
	enable => $service_enable,
	require => Package['ganglia-gmetad'],
    }

    
    service { 'gmond-server':
	name => $service_name,
	ensure => $service_ensure,
	enable => $service_enable,
	require => Package['ganglia-gmond'],
    }

    file {'gmond-server.conf':
        path => "${conf_file}",
	ensure => file,
	owner => 'root',
	group => 'root',
	mode => '0644',
	content => epp('ganglia/etc/ganglia/gmond.conf.epp', {
	    'service_user' => $service_user,
	    'cluster_name' => $cluster_name,
	    'cluster_owner' => $cluster_owner,
	    'cluster_latlong' => $cluster_latlong,
	    'cluster_url' => $cluster_url,
	    'mute' => $mute,
	    'deaf' => $deaf,
	    'location' => $location,
	    'cleanup_threshold' => $cleanup_threshold,
	    'send_metadata_interval' => $send_metadata_interval,
	    'udp_send_channel' => $udp_send_channel,
	    'udp_recv_channel' => $udp_recv_channel,
	    'tcp_accept_channel' => $tcp_accept_channel,
	    'module_path' => $module_path,
	    'host_dmax' => $host_dmax,
	    'host_tmax' => $host_tmax,
	    'allow_extra_data' => $allow_extra_data,
	    'override_hostname' => $override_hostname,
	}),
	require => Package['ganglia-gmond'],
	notify => Service['gmond-server'],
    }

    file {'gmetad.conf':
       path => "/etc/ganglia/gmetad.conf",
       ensure => file,
       owner => 'root',
       group => 'root',
       mode => '0644',
       content => epp('ganglia/etc/ganglia/gmetad.conf.epp', {
           'gridname' => $gridname,
           'gmetad_username' => $gmetad_username,
           'data_source' => $data_source,
           'trusted_hosts' => $trusted_hosts,
       }),
       require => Package['ganglia-gmetad'],
       notify => Service['gmetad'],
    }


    file {'/etc/systemd/system/gmond-server.service':
        content => '[Unit]
Description=Ganglia Monitoring Daemon for LDAS servers
After=multi-user.target

[Service]
Type=forking
ExecStart=/usr/sbin/gmond -c /etc/ganglia/gmond-server.conf 

[Install]
WantedBy=multi-user.target',
        notify => Exec['systemd-reload-gmond-server'],        
    }

    exec {'systemd-reload-gmond-server':
        command => '/bin/systemctl daemon-reload',
        refreshonly => true,
    }



}
